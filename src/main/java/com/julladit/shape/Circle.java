/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shape;

/**
 *
 * @author acer
 */
public class Circle extends Shape {

    private double R;
    static final double pi = 22.0 / 7;

    public Circle(double R) {
        System.out.println("Circle Create");
        this.R = R;
    }

    @Override
    public double calArea() {
        return R * R * pi;
    }
}
