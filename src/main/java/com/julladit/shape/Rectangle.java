/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shape;

/**
 *
 * @author acer
 */
public class Rectangle extends Shape {
    private double w, h;
    public Rectangle(double w, double h) {
        System.out.println("Rectangle Create");
        this.w = w;
        this.h = h;
    }
    @Override
     public double calArea() {
        return w * h;
    }

}
