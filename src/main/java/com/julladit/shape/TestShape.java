/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shape;

/**
 *
 * @author acer
 */
public class TestShape {

    public static void main(String[] args) {
        Shape shape = new Shape();
        System.out.println("-----------");
        Rectangle rectangle = new Rectangle(4, 3);
        System.out.println(rectangle.calArea());
        System.out.println("-----------");
        Square square = new Square(2);
        System.out.println(square.calArea());
        System.out.println("-----------");
        Triangle triangle = new Triangle(3, 4);
        System.out.println(triangle.calArea());
        System.out.println("-----------");
        Circle circle = new Circle(4);
        System.out.println(circle.calArea());
        System.out.println("-----------");
        Circle circle1 = new Circle(3);
        System.out.println(circle1.calArea());
        System.out.println("-----------");
        System.out.println("");
        Shape[] shapes = {rectangle, square, triangle, circle, circle1};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].calArea());
        }

    }
}
