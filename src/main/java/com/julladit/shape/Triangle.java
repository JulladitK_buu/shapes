/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.shape;

/**
 *
 * @author acer
 */
public class Triangle extends Shape {

    private double Base;
    private double Height;
    static final double B = 0.5;

    public Triangle(double Base, double Height) {
        System.out.println("Triangle Create");
        this.Base = Base;
        this.Height = Height;

    }

    @Override
    public double calArea() {
        return Base * Height * B;
    }

}
